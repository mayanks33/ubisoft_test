const Express = require("express");
const app = Express();
const bodyParser = require("body-parser");
const Mongoose = require("mongoose");
const port = process.env.PORT || 5000;
const passport = require("passport");
const interval = require("interval-promise");
const User = require("./models/User");
const Item = require("./models/Item");
//get routes
const load_data = require("./routes/api/load_data");
const getInventory = require("./routes/api/getInventory");
const connect = require("./routes/api/connect");
const getItems = require("./routes/api/getItems");
const getPoints = require("./routes/api/getPoints");
const purchaseItem = require("./routes/api/purchaseItem");
const viewTransactions = require("./routes/api/viewTransactions");
// MongoDB Mlab Connection
const db = require("./config/keys").mongoURI;
Mongoose.connect(
  db,
  { useNewUrlParser: true }
)
  .then(() => {
    console.log("mongoDB connected");
  })
  .catch(err => console.log(err));

//body parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//passport middleware
app.use(passport.initialize());
//passport config
require("./config/passport")(passport);

//use routes
app.use("/load_data", load_data);
app.use("/getPoints", getPoints);
app.use("/purchaseItem", purchaseItem);
app.use("/connect", connect);
app.use("/getInventory", getInventory);
app.use("/getItems", getItems);
app.use("/viewTransactions", viewTransactions);

app.get("/", function(req, res) {
  res.send("Please use API access, View ReadMe for complete API usage details");
});

app.listen(port, () => console.log(`Server running on port ${port}`));
