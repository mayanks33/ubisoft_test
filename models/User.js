const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//The User Schema
const UserSchema = new Schema(
  {
    name: {
      type: String,
      required: true
    },
    email: {
      type: String,
      required: true
    },
    password: {
      type: String,
      required: true
    },
    points_tally:
      //array of all points for the user
      {
        type: [
          {
            pointsfree: {
              type: Number
            },
            pointsbought: {
              type: Number
            },
            totalpoints: {
              type: Number
            }
          }
        ],
        default: [{ pointsfree: 0, pointsbought: 0, totalpoints: 0 }]
      },
    items_tally: [
      {
        itemboughtID: {
          type: Schema.Types.ObjectId,
          ref: "item"
        },
        itemName: {
          type: String
        },
        itemDescription: {
          type: String
        }
      }
    ],
    transactions: [
      {
        name: {
          type: String,
          required: true
        },
        product_name: {
          type: String,
          required: true
        },
        productID: {
          type: Schema.Types.ObjectId,
          ref: "item",
          required: true
        },
        productUnits: {
          type: Number,
          required: true
        },
        timestamp: {
          type: Date,
          default: Date.now()
        }
      }
    ]
  },
  { versionKey: false }
);

module.exports = User = mongoose.model("user", UserSchema);
