const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//GiftToken Schema
const GiftTokenSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  user_email: {
    type: String,
    required: true
  },
  userID: {
    type: Schema.Types.ObjectId,
    ref: "user"
  },
  product_name: {
    type: String,
    required: true
  },
  productID: {
    type: Schema.Types.ObjectId,
    ref: "item",
    required: true
  },
  timestamp: {
    type: Date,
    default: Date.now()
  }
});

module.exports = GiftToken = mongoose.model("giftToken", GiftTokenSchema);
