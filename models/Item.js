const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//The Item List Schema
const ItemSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  cost: {
    type: Number,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  itemtype: {
    //type = 0 for non-points item, type = <value> for the value of points the item gives to the user
    type: Number,
    required: true,
    default: 0
  },
  is_available: {
    type: Boolean,
    required: true
  },
  units_available: {
    type: Number,
    required: true
  }
});

module.exports = Item = mongoose.model("item", ItemSchema);
