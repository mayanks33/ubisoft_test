# Ubisoft_Test

Node,Express & MongoDB Backend Server Deployed using AWS EC2 instance

## Getting Started

Has Dummy Data present and also has a custom route to add dummy data into the database and create new users

## Prerequisites

Any API testing client

```
eg: Postman
```

## Microservice Implemented

The new implementation uses tokens that are issued to eligible users, these tokens save the timestamp with them and whenever the user triggers either the ./getPoints route or makes any transaction through ./purchaseItem, these tokens are looked up from the database and then the difference between the time of issuing them and the current time of request is calculated. This gives us the elapsed time and if that time threshold is eligible for free points the user is awarded free points according to the time elapsed, and the token is updated with a new timestamp if the points are still below a set target or the token is then deleted from the database if the user has reached a certain threshold of free points

## Project Details

Complete list of routes implemented and their functioning along with the database description are listed below

- AWS Access Link -> http://ec2-18-191-187-11.us-east-2.compute.amazonaws.com:5000

### Route list

- ./connect -> http://ec2-18-191-187-11.us-east-2.compute.amazonaws.com:5000/connect
- ./getInventory -> http://ec2-18-191-187-11.us-east-2.compute.amazonaws.com:5000/getInventory
- ./getItems -> http://ec2-18-191-187-11.us-east-2.compute.amazonaws.com:5000/getItems
- ./getPoints -> http://ec2-18-191-187-11.us-east-2.compute.amazonaws.com:5000/getPoints
- ./purchaseItem -> http://ec2-18-191-187-11.us-east-2.compute.amazonaws.com:5000/purchaseItem
- ./viewTransactions -> http://ec2-18-191-187-11.us-east-2.compute.amazonaws.com:5000/viewTransactions
- ./load_data/user -> http://ec2-18-191-187-11.us-east-2.compute.amazonaws.com:5000/load_data/user
- ./load_data/items -> http://ec2-18-191-187-11.us-east-2.compute.amazonaws.com:5000/load_data/items

### Route(s) Description

### (Required Fields are case sensitive)

- ./connect route

```
Type: POST
Description: Login & returns user details like inventory, points etc.
Access: Public
Required Fields: email & password
```

- ./getInventory route

```
Type: GET
Description: Lists all the items available to buy from the backend marketplace
Access: Public
Required Fields: None
```

- ./getItems route

```
Type: GET
Description: Returns the inventory details of the currently logged in user
Access: Protected
Required Fields: JWT Bearer Token as Authorization Header
```

- ./getPoints route

```
Type: GET
Description: Returns the points tally of the currently logged in user
Access: Protected
Required Fields: JWT Bearer Token as Authorization Header
```

- ./purchaseItem route

```
Type: POST
Description: Purchases an item on behalf of the logged in user and updates the respective collections
Access: Protected
Required Fields: JWT Bearer Token as Authorization Header, name(item name, can be read from inventory route) , units(number to buy)
```

- ./viewTransactions route

```
Type: GET
Description: Returns the recorded transaction history
Access: Public
Required Fields: None
```

- ./load_data/items

```
Type: POST
Description: Allows to add Item data
Access: Public
Required Fields: name, cost, description, itemtype, is_available, units_available
```

- ./load_data/user

```
Type: POST
Description: Allows to add user data
Access: Public
Required Fields: name, email, password
```

### Database Design Schemas

- User Schema
  Stores the user's name, email, password, points tally(purchased,free,total), items tally(itemID,item description, item name), and also stores the transaction history of the user

```
{
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  points_tally:
    //array of all points for the user
    {
      type: [
        {
          pointsfree: {
            type: Number
          },
          pointsbought: {
            type: Number
          },
          totalpoints: {
            type: Number
          }
        }
      ],
      default: [{ pointsfree: 0, pointsbought: 0, totalpoints: 0 }]
    },
  items_tally: [
    {
      itemboughtID: {
        type: Schema.Types.ObjectId,
        ref: "item"
      },
      itemName: {
        type: String
      },
      itemDescription: {
        type: String
      }
    }
  ],
    transactions: [
      {
        name: {
          type: String,
          required: true
        },
        product_name: {
          type: String,
          required: true
        },
        productID: {
          type: Schema.Types.ObjectId,
          ref: "item",
          required: true
        },
        productUnits: {
          type: Number,
          required: true
        },
        timestamp: {
          type: Date,
          default: Date.now()
        }
      }
    ]
}
```

- GiftToken Schema
  Stores the gifToken issued to user's to be used at a later stage for redeeming free points automatically when making transactions or checking points tally.

```
{
  name: {
    type: String,
    required: true
  },
  user_email: {
    type: String,
    required: true
  },
  userID: {
    type: Schema.Types.ObjectId,
    ref: "user"
  },
  product_name: {
    type: String,
    required: true
  },
  productID: {
    type: Schema.Types.ObjectId,
    ref: "item",
    required: true
  },
  timestamp: {
    type: Date,
    default: Date.now()
  }
}

```

- Item Schema
  Stores the item's name, cost, description, item type\*, is available flag, units available.
- \*item type -> 0 is for an item which doesn't add to points and is available to purchase through user's points, any value corresponding to the item's description is for denoting an item that adds up in the user's points tally and is purchaseable through external currency.

```
{
  name: {
    type: String,
    required: true
  },
  cost: {
    type: Number,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  itemtype: {
    //type = 0 for non-points item, type = <value> for the value of points the item gives to the user
    type: Number,
    required: true,
    default: 0
  },
  is_available: {
    type: Boolean,
    required: true
  },
  units_available: {
    type: Number,
    required: true
  }
}
```

## Built With

- AWS EC2 Instance
- mLab MongoDB
- Node & Express
- Passport & JWT
- VSCode With Git VCS @ bitbucket

## Author

- **Mayank Srivastava**

## License

This project is licensed under the ISC License
