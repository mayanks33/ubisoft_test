const Validator = require("validator");
const isEmpty = require("./is_empty");

module.exports = function validateRegisterInput(data) {
  let errors = {};

  data.name = !isEmpty(data.name) ? data.name : "";
  data.units = !isEmpty(data.units) ? data.units : "";

  if (Validator.isEmpty(data.name)) {
    errors.name = "Please enter item name";
  }
  if (Validator.isEmpty(data.units)) {
    errors.units = "Please enter units";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
