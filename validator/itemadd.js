const Validator = require("validator");
const isEmpty = require("./is_empty");
module.exports = function validateRegisterInput(data) {
  let errors = {};

  data.name = !isEmpty(data.name) ? data.name : "";
  data.cost = !isEmpty(data.cost) ? data.cost : "";
  data.description = !isEmpty(data.description) ? data.description : "";
  data.itemtype = !isEmpty(data.itemtype) ? data.itemtype : "";
  data.is_available = !isEmpty(data.units) ? data.units : "";
  data.units_available = !isEmpty(data.units) ? data.units : "";

  if (Validator.isEmpty(data.name)) {
    errors.name = "Please enter item name";
  }
  if (Validator.isEmpty(data.cost)) {
    errors.cost = "Please enter cost";
  }
  if (Validator.isEmpty(data.description)) {
    errors.description = "Please enter description";
  }
  if (Validator.isEmpty(data.itemtype)) {
    errors.itemtype = "Please enter itemtype";
  }
  if (Validator.isEmpty(data.is_available)) {
    errors.is_available = "Please enter is_available";
  }
  if (Validator.isEmpty(data.units_available)) {
    errors.units_available = "Please enter units_available";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
