const express = require("express");
const router = express.Router();
const passport = require("passport");
const GiftToken = require("../../models/GiftToken");
const User = require("../../models/User");
const isEmpty = require("../../validator/is_empty");
// @route GET /getPoints
// @desc returns the points of the current user
// @access Protected

const issueToken = user => {
  return GiftToken.findOne({ userID: user.id }).then(token => {
    if (isEmpty(token)) {
      const newGiftToken = new GiftToken({
        name: "Free Points Gift Token",
        user_email: user.email,
        userID: user.id,
        product_name: "Free Points",
        productID: "5c0a87a66b623d3c3832171d",
        timestamp: Date.now()
      });
      newGiftToken.save().then(() => {
        console.log("New Token Issued for " + user.email);
        console.log(
          "\nThe token is " +
            newGiftToken +
            " issued at " +
            newGiftToken.timestamp.toLocaleString
        );
      });
    } else {
      console.log(
        "Cannot Issue new token, token already issued to " +
          user.email +
          " at " +
          token.timestamp.toLocaleString()
      );
    }
  });
};

const redeemToken = user => {
  return GiftToken.findOne({ userID: user.id }).then(token => {
    if (!isEmpty(token)) {
      let timeElapsed =
        (Math.abs(Date.now() - token.timestamp) / (1000 * 60 * 60)) | 0; //whole number in hours;
      //check if user is eligible to redeem token
      if (timeElapsed >= 3) {
        let temp = {};
        temp.points_tally = user.points_tally;
        let pointsfree = temp.points_tally[0].pointsfree;
        let wholeNumber = (timeElapsed / 3) | 0;
        let awardPoints = 50 * wholeNumber;
        pointsfree = pointsfree + awardPoints;
        if (pointsfree > 200) pointsfree = 200;
        temp.points_tally[0].pointsfree = pointsfree;
        temp.points_tally[0].totalpoints =
          temp.points_tally[0].pointsbought + temp.points_tally[0].pointsfree;
        let newTransaction = {
          name: "Free Points Awarded",
          product_name: "Free Points",
          productID: "5c0a87a66b623d3c3832171d",
          productUnits: wholeNumber,
          timestamp: Date.now()
        };
        User.findOneAndUpdate(
          { _id: user.id },
          { $set: temp, $push: { transactions: newTransaction } },
          { new: true }
        ).then(() => {
          let temp = token.timestamp;
          temp = temp.getTime() + wholeNumber * 3 * 60 * 60 * 1000;
          temp = new Date(temp);
          GiftToken.findOneAndUpdate(
            { userID: user.id },
            { $set: { timestamp: temp } },
            { new: true }
          ).then(token => {
            console.log("Free points awarded to user " + user.email);
            console.log(
              "\nNo. of free points awarded are " +
                newTransaction.productUnits * 50
            );
            console.log(
              "\nUpdated token issued on " + token.timestamp.toLocaleString()
            );
          });
        });
      } else {
        console.log(
          "Token cannot be redeemed, you still have " +
            (3 - timeElapsed) +
            " hour(s) to go"
        );
      }
    }
  });
};

const checkToken = user => {
  return GiftToken.findOne({ userID: user.id }).then(token => {
    if (isEmpty(token)) {
      return 0;
    } else return 1;
  });
};

const deleteToken = user => {
  return GiftToken.findOneAndDelete({ userID: user.id }).then(() => {
    console.log(
      "Free Points Limit Reached for " + user.email + " token deleted"
    );
  });
};

router.get(
  "/",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    User.findOne({ _id: req.user.id })
      .then(user => {
        //check if token exists
        //token doesn't exist
        checkToken(user).then(result => {
          if (result == 0) {
            if (user.points_tally[0].pointsfree < 200) {
              issueToken(user).then(response =>
                res.json({
                  name: user.name,
                  email: user.email,
                  points_tally: user.points_tally
                })
              );
            } else if (user.points_tally[0].pointsfree == 200) {
              checkToken(user).then(exists => {
                if (exists == 0) {
                  res.json({
                    name: user.name,
                    email: user.email,
                    points_tally: user.points_tally
                  });
                } else if (exists == 1) {
                  deleteToken(user).then(response =>
                    res.json({
                      name: user.name,
                      email: user.email,
                      points_tally: user.points_tally
                    })
                  );
                }
              });
            }
          } else if (result == 1) {
            if (user.points_tally[0].pointsfree == 200) {
              deleteToken(user).then(() => {
                res.json({
                  name: user.name,
                  email: user.email,
                  points_tally: user.points_tally
                });
              });
            } else {
              redeemToken(user).then(() =>
                res.json({
                  name: user.name,
                  email: user.email,
                  points_tally: user.points_tally
                })
              );
            }
          }
        });
      })
      .catch(err => res.json(err));
  }
);

module.exports = router;
