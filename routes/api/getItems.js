const express = require("express");
const router = express.Router();
const passport = require("passport");

// @route GET /getItems
// @desc Returns the inventory of the current user
// @access Protected
router.get(
  "/",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    res.json({
      msg: "Inventory Details",
      details: {
        id: req.user.id,
        name: req.user.name,
        email: req.user.email,
        item_list: req.user.items_tally
      }
    });
  }
);

module.exports = router;
