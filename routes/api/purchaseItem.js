const express = require("express");
const router = express.Router();
const passport = require("passport");
const Item = require("../../models/Item");
const User = require("../../models/User");
const GiftToken = require("../../models/GiftToken");
//Load Input Validation
const validateRegisterInput = require("../../validator/items");
const isEmpty = require("../../validator/is_empty");
// @route POST /purchaseItem
// @desc purchase an item corresponding to the item ID
// @access Protected

const issueToken = user => {
  return GiftToken.findOne({ userID: user.id }).then(token => {
    if (isEmpty(token)) {
      const newGiftToken = new GiftToken({
        name: "Free Points Gift Token",
        user_email: user.email,
        userID: user.id,
        product_name: "Free Points",
        productID: "5c0a87a66b623d3c3832171d",
        timestamp: Date.now()
      });
      newGiftToken.save().then(() => {
        console.log("New Token Issued for " + user.email);
        console.log(
          "\nThe token is " +
            newGiftToken +
            " issued at " +
            newGiftToken.timestamp.toLocaleString
        );
      });
    } else {
      console.log(
        "Cannot Issue new token, token already issued to " +
          user.email +
          " at " +
          token.timestamp.toLocaleString()
      );
    }
  });
};

const redeemToken = user => {
  return GiftToken.findOne({ userID: user.id }).then(token => {
    if (!isEmpty(token)) {
      let timeElapsed =
        (Math.abs(Date.now() - token.timestamp) / (1000 * 60 * 60)) | 0; //whole number in hours;
      //check if user is eligible to redeem token
      if (timeElapsed >= 3) {
        let temp = {};
        temp.points_tally = user.points_tally;
        let pointsfree = temp.points_tally[0].pointsfree;
        let wholeNumber = (timeElapsed / 3) | 0;
        let awardPoints = 50 * wholeNumber;
        pointsfree = pointsfree + awardPoints;
        if (pointsfree > 200) pointsfree = 200;
        temp.points_tally[0].pointsfree = pointsfree;
        temp.points_tally[0].totalpoints =
          temp.points_tally[0].pointsbought + temp.points_tally[0].pointsfree;
        let newTransaction = {
          name: "Free Points Awarded",
          product_name: "Free Points",
          productID: "5c0a87a66b623d3c3832171d",
          productUnits: wholeNumber,
          timestamp: Date.now()
        };
        User.findOneAndUpdate(
          { _id: user.id },
          { $set: temp, $push: { transactions: newTransaction } },
          { new: true }
        ).then(() => {
          let temp = token.timestamp;
          temp = temp.getTime() + wholeNumber * 3 * 60 * 60 * 1000;
          temp = new Date(temp);
          GiftToken.findOneAndUpdate(
            { userID: user.id },
            { $set: { timestamp: temp } },
            { new: true }
          ).then(token => {
            console.log("Free points awarded to user " + user.email);
            console.log(
              "\nNo. of free points awarded are " +
                newTransaction.productUnits * 50
            );
            console.log(
              "\nUpdated token issued on " + token.timestamp.toLocaleString()
            );
          });
        });
      } else {
        console.log(
          "Token cannot be redeemed, you still have " +
            (3 - timeElapsed) +
            " hour(s) to go"
        );
      }
    }
  });
};

const checkToken = user => {
  return GiftToken.findOne({ userID: user.id }).then(token => {
    if (isEmpty(token)) {
      return 0;
    } else return 1;
  });
};

const deleteToken = user => {
  return GiftToken.findOneAndDelete({ userID: user.id }).then(() => {
    console.log(
      "Free Points Limit Reached for " + user.email + " token deleted"
    );
  });
};

router.post(
  "/",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const { errors, isValid } = validateRegisterInput(req.body);

    //Validation
    if (!isValid) {
      return res.status(400).json(errors);
    } else {
      Item.findOne({ name: req.body.name })
        .then(item => {
          if (
            item.units_available >= req.body.units &&
            item.is_available === true
          ) {
            let ItemFields = {};
            ItemFields.units_available = item.units_available;
            ItemFields.units_available =
              ItemFields.units_available - req.body.units;
            if (ItemFields.units_available === 0) {
              ItemFields.is_available = false;
            }
            //non-points item
            if (item.itemtype === 0) {
              let purchasedItem = {
                itemboughtID: item.id,
                itemName: item.name,
                itemDescription: item.description
              };
              let cost = item.cost * req.body.units;
              //check if free points can be credited first
              User.findOne({ _id: req.user.id })
                .then(user => {
                  if (user.points_tally[0].pointsfree < 200) {
                    //token not found
                    checkToken(user).then(exists => {
                      if (exists == 0) {
                        if (user.points_tally[0].totalpoints >= cost) {
                          let temp = {};
                          const free = user.points_tally[0].pointsfree;
                          const purchased = user.points_tally[0].pointsbought;
                          temp.points_tally = user.points_tally;
                          temp.points_tally[0].pointsfree =
                            temp.points_tally[0].pointsfree - cost;
                          // console.log(cost);
                          // console.log(user.points_tally[0]);
                          cost = cost - free;
                          // console.log(cost);
                          if (temp.points_tally[0].pointsfree < 0) {
                            temp.points_tally[0].pointsfree = 0;
                          }
                          if (cost > 0) {
                            temp.points_tally[0].pointsbought =
                              temp.points_tally[0].pointsbought - cost;
                            cost = cost - purchased;
                            // console.log(cost);
                          }
                          temp.points_tally[0].totalpoints =
                            temp.points_tally[0].pointsbought +
                            temp.points_tally[0].pointsfree;
                          // console.log(cost + " " + temp.points_tally[0]);
                          user.items_tally.unshift(purchasedItem);
                          user.save().then(user => {
                            User.findOneAndUpdate(
                              { _id: req.user.id },
                              { $set: temp },
                              { new: true }
                            )
                              .then(user => {
                                Item.findOneAndUpdate(
                                  { name: item.name },
                                  { $set: ItemFields },
                                  { new: true }
                                )
                                  .then(item => {
                                    //Check if gift card eligible
                                    if (user.points_tally[0].pointsfree < 200) {
                                      issueToken(user).then(() => {
                                        let newTransaction = {
                                          name: "Item Purchased",
                                          user_email: user.email,
                                          userID: user.id,
                                          product_name: item.name,
                                          productID: item.id,
                                          productUnits: req.body.units
                                        };
                                        user.transactions.unshift(
                                          newTransaction
                                        );
                                        user.save().then(user => {
                                          console.log(
                                            "Item Purchased By " + user.email
                                          );
                                          console.log(
                                            "\nTransaction details are " +
                                              newTransaction
                                          );
                                          res.json({
                                            name: user.name,
                                            email: user.email,
                                            items_tally: user.items_tally,
                                            points_tally: user.points_tally,
                                            transactions: user.transactions
                                          });
                                        });
                                      });
                                    } else {
                                      let newTransaction = {
                                        name: "Item Purchased",
                                        user_email: user.email,
                                        userID: user.id,
                                        product_name: item.name,
                                        productID: item.id,
                                        productUnits: req.body.units
                                      };
                                      user.transactions.unshift(newTransaction);
                                      user.save().then(user => {
                                        console.log(
                                          "Item Purchased By " + user.email
                                        );
                                        console.log(
                                          "\nTransaction details are " +
                                            newTransaction
                                        );
                                        res.json({
                                          name: user.name,
                                          email: user.email,
                                          items_tally: user.items_tally,
                                          points_tally: user.points_tally,
                                          transactions: user.transactions
                                        });
                                      });
                                    }
                                  })
                                  .catch(err =>
                                    res.json({
                                      msg: "saving Item failed",
                                      err
                                    })
                                  );
                              })
                              .catch(err =>
                                res.json({ msg: "saving user failed", err })
                              );
                          });
                        } else {
                          res.json({ msg: "Insufficient Points" });
                        }
                      } else if (exists == 1) {
                        redeemToken(user).then(() => {
                          if (user.points_tally[0].totalpoints >= cost) {
                            let temp = {};
                            const free = user.points_tally[0].pointsfree;
                            const purchased = user.points_tally[0].pointsbought;
                            temp.points_tally = user.points_tally;
                            temp.points_tally[0].pointsfree =
                              temp.points_tally[0].pointsfree - cost;
                            // console.log(cost);
                            // console.log(user.points_tally[0]);
                            cost = cost - free;
                            // console.log(cost);
                            if (temp.points_tally[0].pointsfree < 0) {
                              temp.points_tally[0].pointsfree = 0;
                            }
                            if (cost > 0) {
                              temp.points_tally[0].pointsbought =
                                temp.points_tally[0].pointsbought - cost;
                              cost = cost - purchased;
                              // console.log(cost);
                            }
                            temp.points_tally[0].totalpoints =
                              temp.points_tally[0].pointsbought +
                              temp.points_tally[0].pointsfree;
                            // console.log(cost + " " + temp.points_tally[0]);
                            user.items_tally.unshift(purchasedItem);
                            user.save().then(user => {
                              User.findOneAndUpdate(
                                { _id: req.user.id },
                                { $set: temp },
                                { new: true }
                              )
                                .then(user => {
                                  Item.findOneAndUpdate(
                                    { name: item.name },
                                    { $set: ItemFields },
                                    { new: true }
                                  )
                                    .then(item => {
                                      //Check if gift card eligible
                                      if (
                                        user.points_tally[0].pointsfree < 200
                                      ) {
                                        issueToken(user).then(() => {
                                          let newTransaction = {
                                            name: "Item Purchased",
                                            user_email: user.email,
                                            userID: user.id,
                                            product_name: item.name,
                                            productID: item.id,
                                            productUnits: req.body.units
                                          };
                                          user.transactions.unshift(
                                            newTransaction
                                          );
                                          user.save().then(user => {
                                            console.log(
                                              "Item Purchased By " + user.email
                                            );
                                            console.log(
                                              "\nTransaction details are " +
                                                newTransaction
                                            );
                                            res.json({
                                              name: user.name,
                                              email: user.email,
                                              items_tally: user.items_tally,
                                              points_tally: user.points_tally,
                                              transactions: user.transactions
                                            });
                                          });
                                        });
                                      }
                                      if (
                                        user.points_tally[0].pointsfree == 200
                                      ) {
                                        deleteToken(user);
                                      }
                                    })
                                    .catch(err =>
                                      res.json({
                                        msg: "saving Item failed",
                                        err
                                      })
                                    );
                                })
                                .catch(err =>
                                  res.json({ msg: "saving user failed", err })
                                );
                            });
                          } else {
                            res.json({ msg: "Insufficient Points" });
                          }
                        });
                      }
                    });
                  } else {
                    if (user.points_tally[0].totalpoints >= cost) {
                      let temp = {};
                      const free = user.points_tally[0].pointsfree;
                      const purchased = user.points_tally[0].pointsbought;
                      temp.points_tally = user.points_tally;
                      temp.points_tally[0].pointsfree =
                        temp.points_tally[0].pointsfree - cost;
                      // console.log(cost);
                      // console.log(user.points_tally[0]);
                      cost = cost - free;
                      // console.log(cost);
                      if (temp.points_tally[0].pointsfree < 0) {
                        temp.points_tally[0].pointsfree = 0;
                      }
                      if (cost > 0) {
                        temp.points_tally[0].pointsbought =
                          temp.points_tally[0].pointsbought - cost;
                        cost = cost - purchased;
                        // console.log(cost);
                      }
                      temp.points_tally[0].totalpoints =
                        temp.points_tally[0].pointsbought +
                        temp.points_tally[0].pointsfree;
                      // console.log(cost + " " + temp.points_tally[0]);
                      user.items_tally.unshift(purchasedItem);
                      user.save().then(user => {
                        User.findOneAndUpdate(
                          { _id: req.user.id },
                          { $set: temp },
                          { new: true }
                        )
                          .then(user => {
                            Item.findOneAndUpdate(
                              { name: item.name },
                              { $set: ItemFields },
                              { new: true }
                            )
                              .then(item => {
                                //Check if gift card eligible
                                if (user.points_tally[0].pointsfree < 200) {
                                  issueToken(user).then(() => {
                                    let newTransaction = {
                                      name: "Item Purchased",
                                      user_email: user.email,
                                      userID: user.id,
                                      product_name: item.name,
                                      productID: item.id,
                                      productUnits: req.body.units
                                    };
                                    user.transactions.unshift(newTransaction);
                                    user.save().then(user => {
                                      console.log(
                                        "Item Purchased By " + user.email
                                      );
                                      console.log(
                                        "\nTransaction details are " +
                                          newTransaction
                                      );
                                      res.json({
                                        name: user.name,
                                        email: user.email,
                                        items_tally: user.items_tally,
                                        points_tally: user.points_tally,
                                        transactions: user.transactions
                                      });
                                    });
                                  });
                                } else {
                                  let newTransaction = {
                                    name: "Item Purchased",
                                    user_email: user.email,
                                    userID: user.id,
                                    product_name: item.name,
                                    productID: item.id,
                                    productUnits: req.body.units
                                  };
                                  user.transactions.unshift(newTransaction);
                                  user.save().then(user => {
                                    console.log(
                                      "Item Purchased By " + user.email
                                    );
                                    console.log(
                                      "\nTransaction details are " +
                                        newTransaction
                                    );
                                    res.json({
                                      name: user.name,
                                      email: user.email,
                                      items_tally: user.items_tally,
                                      points_tally: user.points_tally,
                                      transactions: user.transactions
                                    });
                                  });
                                }
                              })
                              .catch(err =>
                                res.json({
                                  msg: "saving Item failed",
                                  err
                                })
                              );
                          })
                          .catch(err =>
                            res.json({ msg: "saving user failed", err })
                          );
                      });
                    } else {
                      res.json({ msg: "Insufficient Points" });
                    }
                  }
                })
                .catch(err => res.json(err));
            }
            //points purchased
            else {
              User.findOne({ _id: req.user.id })
                .then(user => {
                  var temp = {};
                  temp.points_tally = user.points_tally;
                  var value = item.itemtype;
                  value = value * req.body.units;
                  temp.points_tally[0].pointsbought =
                    temp.points_tally[0].pointsbought + value;
                  temp.points_tally[0].totalpoints =
                    temp.points_tally[0].pointsfree +
                    temp.points_tally[0].pointsbought;

                  User.findOneAndUpdate(
                    { _id: user.id },
                    { $set: temp },
                    { new: true }
                  )
                    .then(user =>
                      redeemToken(user)
                        .then(() => {
                          Item.findOneAndUpdate(
                            { name: item.name },
                            { $set: ItemFields },
                            { new: true }
                          )
                            .then(item => {
                              const newTransaction = {
                                name: "Points Purchased",
                                product_name: item.name,
                                productID: item.id,
                                productUnits: req.body.units
                              };
                              user.transactions.unshift(newTransaction);
                              user.save().then(() => {
                                console.log(
                                  "Points Purchased By " + user.email
                                );
                                console.log(
                                  "\nTransaction details are " + newTransaction
                                );
                                User.findOne({ _id: user.id }).then(user => {
                                  res.json({
                                    name: user.name,
                                    email: user.email,
                                    items_tally: user.items_tally,
                                    points_tally: user.points_tally,
                                    transactions: user.transactions
                                  });
                                });
                              });
                            })
                            .catch(err =>
                              res.json({ msg: "saving Item failed", err })
                            );
                        })
                        .catch(err => res.json(err))
                    )
                    .catch(err => res.json(err));
                })
                .catch(err => res.json(err));
            }
          } else {
            if (!item.is_available) {
              res.json({ msg: "Item not available for purchase" });
            } else
              res.json({
                msg: "Units Requested exceeds units available in store"
              });
          }
        })
        .catch(err => res.json({ msg: "item not found", err }));
    }
  }
);

module.exports = router;
