const express = require("express");
const router = express.Router();
const Item = require("../../models/Item");

// @route GET /getInventory
// @desc Gets the list of all items available to buy
// @access Public
router.get("/", (req, res) => {
  Item.find({ is_available: true })
    .then(items => {
      var ItemList = [];
      items.forEach(item => {
        var item = {
          ItemID: item.id,
          Item_Name: item.name,
          Item_Description: item.description,
          Item_Cost: item.cost,
          Units_Available: item.units_available
        };
        ItemList.push(item);
      });
      res.json({ msg: "The Marketplace Inventory", ItemList });
    })
    .catch(err => res.json(err));
});

module.exports = router;
