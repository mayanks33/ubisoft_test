const express = require("express");
const router = express.Router();
const User = require("../../models/User");
const passport = require("passport");
// @route GET /viewTransactions
// @desc Gets the list of all transactions recorded
// @access Private
router.get(
  "/",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    User.findOne({ _id: req.user.id }).then(user => {
      res.json({
        msg: "The transaction history of the user",
        name: user.name,
        email: user.email,
        transactions: user.transactions
      });
    });
  }
);

module.exports = router;
