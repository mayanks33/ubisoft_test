const express = require("express");
const router = express.Router();
const User = require("../../models/User");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const keys = require("../../config/keys").secretOrKey;

//Load Input Validation
const validateRegisterInput = require("../../validator/login");

// @route POST /connect
// @desc Login & return user details like inventory, points etc.
// @access Public
router.post("/", (req, res) => {
  const { errors, isValid } = validateRegisterInput(req.body);

  //Validation
  if (!isValid) {
    return res.status(400).json(errors);
  }
  User.findOne({ email: req.body.email })
    .then(user => {
      if (user) {
        bcrypt.compare(req.body.password, user.password).then(matched => {
          if (matched) {
            // JWT Payload
            const payload = { id: user.id, name: user.name, email: user.email };
            //Sign JWT
            //Token Expiry is set to 3600 seconds = 1 Hour
            jwt.sign(payload, keys, { expiresIn: 3600 }, (err, token) => {
              res.json({
                Name: user.name,
                Email: user.email,
                Points: user.points_tally,
                Items: user.items_tally,
                login_success: true,
                token: "Bearer " + token
              });
            });
          } else {
            errors.password = "Passwords Do Not Match";
            return res.status(400).json(errors);
          }
        });
      } else {
        errors.email = "User Email Not Found";
        res.status(404).json(errors);
      }
    })
    .catch(err => res.json(err));
});

module.exports = router;
