const express = require("express");
const router = express.Router();
const User = require("../../models/User");
const Item = require("../../models/Item");
const bcrypt = require("bcrypt");

const validateRegisterInput = require("../../validator/useradd");
const vRI = require("../../validator/itemadd");
// @route POST /load_data/user
// @desc Add user data
// @access Public
router.post("/user", (req, res) => {
  const { errors, isValid } = validateRegisterInput(req.body);

  //Validation
  if (!isValid) {
    return res.status(400).json(errors);
  }
  User.findOne({ email: req.body.email })
    .then(user => {
      if (user) {
        return res.status(400).json({ msg: "Email already exists" });
      } else {
        const newUser = new User({
          name: req.body.name,
          email: req.body.email,
          password: "placeholder"
        });
        bcrypt.genSalt(10, (err, salt) => {
          bcrypt.hash(req.body.password, salt, (err, hash) => {
            if (err) {
              throw err;
            }
            newUser.password = hash;
            newUser
              .save()
              .then(user => res.json(user))
              .catch(err => {
                res.json(err);
              });
          });
        });
      }
    })
    .catch(err => {
      res.json(err);
    });
});

// @route POST /load_data/items
// @desc Add item data
// @access Public
router.post("/items", (req, res) => {
  const { errors, isValid } = vRI(req.body);

  //Validation
  if (!isValid) {
    return res.status(400).json(errors);
  }
  Item.findOne({ name: req.body.name }).then(item => {
    if (item) {
      res.status(400).json({ msg: "Item already exists" });
    } else {
      const newItem = new Item({
        name: req.body.name,
        cost: req.body.cost,
        description: req.body.description,
        itemtype: req.body.itemtype,
        is_available: req.body.is_available,
        units_available: req.body.units_available
      });
      newItem.save();
      res.json(newItem);
    }
  });
});

module.exports = router;
